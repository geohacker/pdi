f = zeros(3,3);
f(2:2,2:2) = 1;

F = fft2(f) %Transformada de fourier
F_real = real(F) %Hallar la parte real de la matriz F
F_i = imag(F)%Hallar la parte imaginaria de la matriz F
F_ang = angle(F)%Hallar el Angulo 
F_mag = abs(F)%Hallar la Magnitud
F_inversa = ifft2(F) %Inversa de la T. de Fourier  

%Leer Letra
letra = imread('i.tif')
b1_letra = letra(:,:,1);
letraF = fft2(double(b1_letra));
figure, imagesc(log(fftshift(abs(letraF)))+1); colormap gray;