%Genera una matriz aleatoria 5x5 con valores entre 0 a 255
n = 5 
m = 5
matrizA = randi([0 255], m,n) 
matrizB = randi([0 255], m,n) 

%Matrices Bases
matrizA = [
     4    13    26   137   231; 
    173   146   105   169    35;
     54    37   177   131    35;
     67   150   106   241   206;
     125   179    12   150   101
    ]

matrizB = [
      42   226   229    29    60; 
    237   159   109   243   231;
     89   192   246   115   146;
     192    89   169   148     0;
    185    69   159   104   157
    ]

%Convertir las matrices a 8bits
matrizA8 = uint8(matrizA)
matrizB8 = uint8(matrizB)

%Guardar matrices como imagenes en el folder img

imwrite(matrizA8, '../img/operaciones/imgA8.tif')
imwrite(matrizB8, '../img/operaciones/imgB8.tif')

%Mostrar las imagenes
figure, imshow(matrizA8, 'InitialMagnification', 'fit')
%figure, imshow(matrizB8, 'InitialMagnification', 'fit')
