%Matrices Ejemplo
A = [
     4    13    26   137   231; 
    173   146   105   169    35;
     54    37   177   131    35;
     67   150   106   241   206;
     125   179    12   150   101
    ]

B = [
      42   226   229    29    60; 
    237   159   109   243   231;
     89   192   246   115   146;
     192    89   169   148     0;
    185    69   159   104   157
    ]



for i = 1:5;
    for j = 1:5;
        %Multiplicación mij
        M(i,j) = (A(i,j)*B(i,j))/255;  
        %Generar Matriz de Resultado C
        if (M(i,j) >= 0 && M(i,j) <=1); %Normalización Caso1
            C(i,j) = M(i,j)*127+1;
        else 
            C(i,j) = (M(i,j)/2)+128; %Normalización Caso2
        end
    end
end

CM = fix(C)
CM8 = uint8(CM)
imwrite(CM8, '../img/operaciones/imgC8-M.tif')

%Mostrar las imagenes
figure, imshow(CM8, 'InitialMagnification', 'fit')






