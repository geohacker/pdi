
%Leer los ND de la imagen
imgA = imread('../img/operaciones/imgA8.tif');
imgB = imread('../img/operaciones/imgB8.tif');

%Dado que la imgA y imgB contiene valores de 8bits (Por alguna razón la suma no esta muy bien definida), 
%es necesario definir nuevamente a y b para que los valores sean float 
a = [
     4    13    26   137   231; 
    173   146   105   169    35;
     54    37   177   131    35;
     67   150   106   241   206;
     125   179    12   150   101
    ]

b = [
      42   226   229    29    60; 
    237   159   109   243   231;
     89   192   246   115   146;
     192    89   169   148     0;
    185    69   159   104   157
    ]


CS = fix((a+b)/2)
CS8 = uint8(CS)
%imwrite(CS8, '../img/operaciones/imgC8-S.tif')

%Mostrar las imagenes
figure, imshow(CS8, 'InitialMagnification', 'fit')



