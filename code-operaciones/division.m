%Matrices Ejemplo
A = [
     4    13    26   137   231; 
    173   146   105   169    35;
     54    37   177   131    35;
     67   150   106   241   206;
     125   179    12   150   101
    ]

B = [
      42   226   229    29    60; 
    237   159   109   243   231;
     89   192   246   115   146;
     192    89   169   148     0;
    185    69   159   104   157
    ]


%División
for i = 1:5;
    for j = 1:5;
        %Generar Matriz División D
        if B(i,j)== 0
           D(i,j) = A(i,j) 
        else 
           D(i,j) =  A(i,j) / B(i,j); 
        end
        %Generar Matriz de Resultado C
        if (D(i,j) >= 0 && D(i,j) <=1); %Normalización Caso1
            C(i,j) = D(i,j)*127+1;
        else 
            C(i,j) = (D(i,j)/2)+128; %Normalización Caso2
        end
    end
end


CD = fix(C)
CD8 = uint8(CD)
imwrite(CD8, '../img/operaciones/imgC8-D.tif')

%Mostrar las imagenes
figure, imshow(CD8, 'InitialMagnification', 'fit')




