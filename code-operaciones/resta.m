%Matrices Ejemplo
a = [
     4    13    26   137   231; 
    173   146   105   169    35;
     54    37   177   131    35;
     67   150   106   241   206;
     125   179    12   150   101
    ]

b = [
      42   226   229    29    60; 
    237   159   109   243   231;
     89   192   246   115   146;
     192    89   169   148     0;
    185    69   159   104   157
    ]


%Restar las bandas
CR = fix((a-b+255)/2)
CR8 = uint8(CR)
imwrite(CR8, '../img/operaciones/imgC8-R.tif')

%Mostrar las imagenes
figure, imshow(CR8, 'InitialMagnification', 'fit')



