img_rgb = 'boa-mulrgb.tif'
img_pan = 'boa-pan.tif'
img_hsv = 'boa-hsv.tif'
img_hspv = 'boa-hspv.tif'

rgb_hsv =  'boa-rgb-hsv.tif'
rgb_hspv = 'boa-rgb-hspv.tif'

img_v = 'boa-v.tif'
img_h = 'boa-h.tif'
img_s = 'boa-s.tif'

[mul, R] = geotiffread(img_rgb);
info_mul = geotiffinfo(img_rgb);
[pan, R] = geotiffread(img_pan);
info_pan = geotiffinfo(img_pan);
%figure, imshow(pan), title('Pancromatica');

%Convertir la imagen a HSV
mul_hsv = rgb2hsv(double(mul));
imwrite(mul_hsv,img_hsv);
figure, imshow(mul_hsv), title('Multiespectral HSV');

%Ver las imanges por separado Tono, Saturación, Value
v=mul_hsv(:,:,3);
s=mul_hsv(:,:,2);
h=mul_hsv(:,:,1);
imwrite(v,img_v);
imwrite(s,img_s);
imwrite(h,img_h);

%v8=uint8(v)

%%Sustitución directa
pan8 = double(pan)
img_hsp = cat(3,h,s,pan8)
img_rgb_hsp = hsv2rgb(img_hsp)
img_rgb_hsp8 = uint8(img_rgb_hsp)
geotiffwrite(rgb_hsv, img_rgb_hsp8 , R, 'GeoKeyDirectoryTag', info_mul.GeoTIFFTags.GeoKeyDirectoryTag);
figure, imshow(img_rgb_hsp8), title('Imagen HSV (H,S,PAN)');



%Realizar un Histogram Matching Ajustas Pancromatica a Value
hm_panv = imhistmatch(pan,v)
hm_panv = double(hm_panv)
img_hspv = cat(3,h,s,hm_panv)
img_rgb_hspv = hsv2rgb(img_hspv)
rgb_hspv8b = uint8(img_rgb_hspv)
geotiffwrite(rgb_hspv,rgb_hspv8b, R, 'GeoKeyDirectoryTag', info_mul.GeoTIFFTags.GeoKeyDirectoryTag);
figure, imshow(img_rgb_hsp8), title('Imagen HSV (H,S,PAN-V)');
