clear
clc
% Laydi Viviana Bautista - 20151025069

% Cargar Imagen Diferente
%file_pan=input ( 'nombre Imagen pancromatica =', 's');
%file_multi=input('nombre Imagen RGB =','s');
%name_wavelet=input ( 'Wavelet =', 's');
%level_wavelet=input('Level =','s');

%haar
%db2’,  ’db5’,  ’db7’,’db8
%’coif2’, ’coif5
%sym3’,’sym6’,’sym7
%’fk6’, ’fk18’
%’dmey’
%’bior1.3’,   ’bior2.6’,’bior3.5’,   ’bior4.4’.’bior5.5
%rbio1.3’,   ’rbio2.6’,’rbio3.5’,   ’rbio4.4’,’rbio5.5

%Imagenes por Default:
file_multi = 'ls-mulrgb.tif';
file_pan = 'ls-pan.tif';
name_wavelet='haar';
level_wavelet=1;
file_output = name_wavelet+"-"+string(level_wavelet)+'.tif';


%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% 1. Carga de Imagenes 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
pan=imread(file_pan);
mul=imread(file_multi);
%figure, imshow(pan), title('Imagen Pancromatica');
%figure, imshow(mul), title('Imagen RGB');
% New Pan
%npan=cat(3,pan, pan, pan);
%imwrite(npan, 'ls-pan-3.tif');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% 2. Transformación RGB A HVS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
hsv=rgb2hsv(mul);
%figure, imshow(hsv), title('Imagen HSI');
val = hsv(:,:,3);
sat = hsv(:,:,2);
hol = hsv(:,:,1);
%figure, imshow(val), title('Imagen Value');
%figure, imshow(sat), title('Imagen Saturacion');
%figure, imshow(hol), title('Imagen Tono');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% 3. Transformación TWD
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Indexar Imagenes
[x,map]=gray2ind(pan,256);
[y,mpp]=gray2ind(val,256);
% TWD Pancromatica
[C,S] = wavedec2(x,level_wavelet,name_wavelet);
cH = detcoef2('h',C,S,level_wavelet); %Coef. Detail Horizontal
cV = detcoef2('v',C,S,level_wavelet); %Coef. Detail Vertical
cD = detcoef2('d',C,S,level_wavelet); %Coef. Detail Diagonal
% TWD Multiespectral
[J,M] = wavedec2(y,level_wavelet,name_wavelet);
AV = appcoef2(J,M,name_wavelet, level_wavelet); %Coef. Aproximanación

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% 3. Concatenar TWD
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
a=S(1,1);
AVr=  reshape(AV,1,[]);

if level_wavelet == 1
    CHr = reshape(cH,1, []);
    CVr = reshape(cV,1, []);
    CDr = reshape(cD,1, []);
    twd_panv = cat(2,AVr, CHr, CVr,CDr);
end

if level_wavelet == 2
    cH1=detcoef2('h',C,S,1);
    cV1=detcoef2('v',C,S,1);
    cD1=detcoef2('d',C,S,1);
    cH2=detcoef2('h', C,S,2); 
    cV2=detcoef2('v', C,S,2); 
    cD2=detcoef2('d', C,S,2); 
    b=S(3,1);
    cH1nueva=reshape(cH1,1,b*b);
    cV1nueva=reshape(cV1,1,b*b);
    cD1nueva=reshape(cD1,1,b*b);
    cH2nueva=reshape(cH2,1,a*a); 
    cV2nueva=reshape(cV2,1,a*a); 
    cD2nueva=reshape(cD2,1,a*a); 
    twd_panv=cat(2,AVr, cH2nueva,cV2nueva,cD2nueva,cH1nueva,cV1nueva,cD1nueva);
end


if level_wavelet == 3
    cH1=detcoef2('h',C,S,1);
    cV1=detcoef2('v',C,S,1);
    cD1=detcoef2('d',C,S,1);
    cH2=detcoef2('h', C,S,2); 
    cV2=detcoef2('v', C,S,2); 
    cD2=detcoef2('d', C,S,2); 
    cH3=detcoef2('h',C,S,3); 
    cV3=detcoef2('v',C,S,3); 
    cD3=detcoef2('d',C,S,3);
    b=S(3,1);
    c=S(4,1); 
    cH1nueva=reshape(cH1,1,c*c);
    cV1nueva=reshape(cV1,1,c*c);
    cD1nueva=reshape(cD1,1,c*c);
    cH2nueva=reshape(cH2,1,b*b); 
    cV2nueva=reshape(cV2,1,b*b); 
    cD2nueva=reshape(cD2,1,b*b); 
    cH3nueva=reshape(cH3,1,a*a); 
    cV3nueva=reshape(cV3,1,a*a); 
    cD3nueva=reshape(cD3,1,a*a);
    twd_panv=cat(2,AVr,cH3nueva,cV3nueva,cD3nueva,cH2nueva,cV2nueva,cD2nueva,cH1nueva,cV1nueva,cD1nueva);
end

if level_wavelet == 4
    cH1=detcoef2('h',C,S,1);
    cV1=detcoef2('v',C,S,1);
    cD1=detcoef2('d',C,S,1);
    cH2=detcoef2('h',C,S,2); 
    cV2=detcoef2('v',C,S,2); 
    cD2=detcoef2('d',C,S,2); 
    cH3=detcoef2('h',C,S,3); 
    cV3=detcoef2('v',C,S,3); 
    cD3=detcoef2('d',C,S,3); 
    cH4=detcoef2('h',C,S,4); 
    cV4=detcoef2('v',C,S,4); 
    cD4=detcoef2('d',C,S,4);
    b=S(3,1);
    c=S(4,1); 
    d=S(5,1);
    cH1nueva=reshape(cH1,1,d*d); 
    cV1nueva=reshape(cV1,1,d*d); 
    cD1nueva=reshape(cD1,1,d*d); 
    cH2nueva=reshape(cH2,1,c*c); 
    cV2nueva=reshape(cV2,1,c*c); 
    cD2nueva=reshape(cD2,1,c*c); 
    cH3nueva=reshape(cH3,1,b*b); 
    cV3nueva=reshape(cV3,1,b*b); 
    cD3nueva=reshape(cD3,1,b*b); 
    cH4nueva=reshape(cH4,1,a*a); 
    cV4nueva=reshape(cV4,1,a*a); 
    cD4nueva=reshape(cD4,1,a*a); 
    twd_panv=cat(2,AVr,cH4nueva,cV4nueva,cD4nueva,cH3nueva,cV3nueva,cD3nueva,cH2nueva,cV2nueva,cD2nueva,cH1nueva,cV1nueva,cD1nueva);
end


if level_wavelet == 5
    cH1=detcoef2('h',C,S,1);
    cV1=detcoef2('v',C,S,1);
    cD1=detcoef2('d',C,S,1);
    cH2=detcoef2('h', C,S,2); 
    cV2=detcoef2('v', C,S,2); 
    cD2=detcoef2('d', C,S,2); 
    cH3=detcoef2('h',C,S,3); 
    cV3=detcoef2('v',C,S,3); 
    cD3=detcoef2('d',C,S,3); 
    cH4=detcoef2('h',C,S,4); 
    cV4=detcoef2('v',C,S,4); 
    cD4=detcoef2('d',C,S,4); 
    cH5=detcoef2('h',C,S,5); 
    cV5=detcoef2('v',C,S,5); 
    cD5=detcoef2('d',C,S,5);
    b=S(3,1);
    c=S(4,1); 
    d=S(5,1);
    e=S(6,1);
    cH1nueva=reshape(cH1,1,e*e); 
    cV1nueva=reshape(cV1,1,e*e); 
    cD1nueva=reshape(cD1,1,e*e); 
    cH2nueva=reshape(cH2,1,d*d); 
    cV2nueva=reshape(cV2,1,d*d); 
    cD2nueva=reshape(cD2,1,d*d); 
    cH3nueva=reshape(cH3,1,c*c); 
    cV3nueva=reshape(cV3,1,c*c); 
    cD3nueva=reshape(cD3,1,c*c); 
    cH4nueva=reshape(cH4,1,b*b); 
    cV4nueva=reshape(cV4,1,b*b); 
    cD4nueva=reshape(cD4,1,b*b); 
    cH5nueva=reshape(cH5,1,a*a); 
    cV5nueva=reshape(cV5,1,a*a); 
    cD5nueva=reshape(cD5,1,a*a); 
    twd_panv=cat(2,AVr,cH5nueva,cV5nueva,cD5nueva,cH4nueva,cV4nueva,cD4nueva,cH3nueva,cV3nueva,cD3nueva,cH2nueva,cV2nueva,cD2nueva,cH1nueva,cV1nueva,cD1nueva); 
end

if level_wavelet == 6   
    cH1=detcoef2('h',C,S,1);
    cV1=detcoef2('v',C,S,1);
    cD1=detcoef2('d',C,S,1);
    cH2=detcoef2('h', C,S,2); 
    cV2=detcoef2('v', C,S,2); 
    cD2=detcoef2('d', C,S,2); 
    cH3=detcoef2('h',C,S,3); 
    cV3=detcoef2('v',C,S,3); 
    cD3=detcoef2('d',C,S,3); 
    cH4=detcoef2('h',C,S,4); 
    cV4=detcoef2('v',C,S,4); 
    cD4=detcoef2('d',C,S,4); 
    cH5=detcoef2('h',C,S,5); 
    cV5=detcoef2('v',C,S,5); 
    cD5=detcoef2('d',C,S,5); 
    cH6=detcoef2('h',C,S,6); 
    cV6=detcoef2('v',C,S,6); 
    cD6=detcoef2('d',C,S,6); 
    b=S(3,1);
    c=S(4,1); 
    d=S(5,1);
    e=S(6,1);
    f=S(7,1);
    cH1nueva=reshape(cH1,1, f*f); 
    cV1nueva=reshape(cV1,1, f*f); 
    cD1nueva=reshape(cD1,1,f*f); 
    cH2nueva=reshape(cH2,1, e*e); 
    cV2nueva=reshape(cV2,1, e*e); 
    cD2nueva=reshape(cD2,1, e*e); 
    cH3nueva=reshape(cH3,1, d*d); 
    cV3nueva=reshape(cV3,1, d*d); 
    cD3nueva=reshape(cD3,1, d*d); 
    cH4nueva=reshape(cH4,1, c*c); 
    cV4nueva=reshape(cV4,1, c*c); 
    cD4nueva=reshape(cD4,1, c*c); 
    cH5nueva=reshape(cH5,1, b*b); 
    cV5nueva=reshape(cV5,1,b*b); 
    cD5nueva=reshape(cD5,1, b*b); 
    cH6nueva=reshape(cH6,1,a*a); 
    cV6nueva=reshape(cV6,1,a*a); 
    cD6nueva=reshape(cD6,1,a*a); 
    twd_panv=cat(2,AVr,cH6nueva,cV6nueva,cD6nueva,cH5nueva,cV5nueva,cD5nueva,cH4nueva,cV4nueva,cD4nueva,cH3nueva,cV3nueva,cD3nueva,cH2nueva,cV2nueva,cD2nueva,cH1nueva,cV1nueva,cD1nueva); 
end

if level_wavelet == 7
    cH1=detcoef2('h',C,S,1);
    cV1=detcoef2('v',C,S,1);
    cD1=detcoef2('d',C,S,1);
    cH2=detcoef2('h', C,S,2); 
    cV2=detcoef2('v', C,S,2); 
    cD2=detcoef2('d', C,S,2); 
    cH3=detcoef2('h',C,S,3); 
    cV3=detcoef2('v',C,S,3); 
    cD3=detcoef2('d',C,S,3); 
    cH4=detcoef2('h',C,S,4); 
    cV4=detcoef2('v',C,S,4); 
    cD4=detcoef2('d',C,S,4); 
    cH5=detcoef2('h',C,S,5); 
    cV5=detcoef2('v',C,S,5); 
    cD5=detcoef2('d',C,S,5); 
    cH6=detcoef2('h',C,S,6); 
    cV6=detcoef2('v',C,S,6); 
    cD6=detcoef2('d',C,S,6); 
    cH7=detcoef2('h',C,S,7); 
    cV7=detcoef2('v',C,S,7); 
    cD7=detcoef2('d',C,S,7);
    b=S(3,1);
    c=S(4,1); 
    d=S(5,1);
    e=S(6,1);
    f=S(7,1);
    g=S(8,1);
    cH1nueva=reshape(cH1,1, g*g); 
    cV1nueva=reshape(cV1,1, g*g); 
    cD1nueva=reshape(cD1,1,g*g); 
    cH2nueva=reshape(cH2,1, f*f); 
    cV2nueva=reshape(cV2,1, f*f); 
    cD2nueva=reshape(cD2,1, f*f); 
    cH3nueva=reshape(cH3,1, e*e); 
    cV3nueva=reshape(cV3,1, e*e); 
    cD3nueva=reshape(cD3,1, e*e); 
    cH4nueva=reshape(cH4,1, d*d); 
    cV4nueva=reshape(cV4,1, d*d); 
    cD4nueva=reshape(cD4,1, d*d); 
    cH5nueva=reshape(cH5,1, c*c); 
    cV5nueva=reshape(cV5,1, c*c); 
    cD5nueva=reshape(cD5,1, c*c); 
    cH6nueva=reshape(cH6,1, b*b); 
    cV6nueva=reshape(cV6,1, b*b); 
    cD6nueva=reshape(cD6,1, b*b); 
    cH7nueva=reshape(cH7,1, a*a); 
    cV7nueva=reshape(cV7,1, a*a); 
    cD7nueva=reshape(cD7,1, a*a);
    twd_panv=cat(2,AVr,cH7nueva,cV7nueva,cD7nueva,cH6nueva,cV6nueva,cD6nueva,cH5nueva,cV5nueva,cD5nueva,cH4nueva,cV4nueva,cD4nueva,cH3nueva,cV3nueva,cD3nueva,cH2nueva,cV2nueva,cD2nueva,cH1nueva,cV1nueva,cD1nueva); 
end

if level_wavelet == 8
    cH1=detcoef2('h',C,S,1);
    cV1=detcoef2('v',C,S,1);
    cD1=detcoef2('d',C,S,1);
    cH2=detcoef2('h', C,S,2); 
    cV2=detcoef2('v', C,S,2); 
    cD2=detcoef2('d', C,S,2); 
    cH3=detcoef2('h',C,S,3); 
    cV3=detcoef2('v',C,S,3); 
    cD3=detcoef2('d',C,S,3); 
    cH4=detcoef2('h',C,S,4); 
    cV4=detcoef2('v',C,S,4); 
    cD4=detcoef2('d',C,S,4); 
    cH5=detcoef2('h',C,S,5); 
    cV5=detcoef2('v',C,S,5); 
    cD5=detcoef2('d',C,S,5); 
    cH6=detcoef2('h',C,S,6); 
    cV6=detcoef2('v',C,S,6); 
    cD6=detcoef2('d',C,S,6); 
    cH7=detcoef2('h',C,S,7); 
    cV7=detcoef2('v',C,S,7); 
    cD7=detcoef2('d',C,S,7);
    cH8=detcoef2('h',C,S,8); 
    cV8=detcoef2('v',C,S,8); 
    cD8=detcoef2('d',C,S,8);
    b=S(3,1);
    c=S(4,1); 
    d=S(5,1);
    e=S(6,1);
    f=S(7,1);
    g=S(8,1);
    h=S(9,1);
    cH1nueva=reshape(cH1,1, h*h); 
    cV1nueva=reshape(cV1,1, h*h); 
    cD1nueva=reshape(cD1,1, h*h); 
    cH2nueva=reshape(cH2,1, g*g); 
    cV2nueva=reshape(cV2,1, g*g); 
    cD2nueva=reshape(cD2,1, g*g); 
    cH3nueva=reshape(cH3,1, f*f); 
    cV3nueva=reshape(cV3,1, f*f); 
    cD3nueva=reshape(cD3,1, f*f); 
    cH4nueva=reshape(cH4,1, e*e); 
    cV4nueva=reshape(cV4,1, e*e); 
    cD4nueva=reshape(cD4,1, e*e); 
    cH5nueva=reshape(cH5,1, d*d); 
    cV5nueva=reshape(cV5,1, d*d); 
    cD5nueva=reshape(cD5,1, d*d); 
    cH6nueva=reshape(cH6,1, c*c); 
    cV6nueva=reshape(cV6,1, c*c); 
    cD6nueva=reshape(cD6,1, c*c); 
    cH7nueva=reshape(cH7,1, b*b); 
    cV7nueva=reshape(cV7,1, b*b); 
    cD7nueva=reshape(cD7,1, b*b); 
    cH8nueva=reshape(cH8,1, a*a); 
    cV8nueva=reshape(cV8,1, a*a); 
    cD8nueva=reshape(cD8,1, a*a);
    twd_panv=cat(2,AVr,cH8nueva,cV8nueva,cD8nueva,cH7nueva,cV7nueva,cD7nueva,cH6nueva,cV6nueva,cD6nueva,cH5nueva,cV5nueva,cD5nueva,cH4nueva,cV4nueva,cD4nueva,cH3nueva,cV3nueva,cD3nueva,cH2nueva,cV2nueva,cD2nueva,cH1nueva,cV1nueva,cD1nueva); 
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% 4. TWD Inversa
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i_twd_panv  = waverec2(twd_panv, S, name_wavelet);
val_new=mat2gray(i_twd_panv);

%%%%%%%%%%%%%%%%%%%%%%%%
%%%% 5. Nueva HSV
%%%%%%%%%%%%%%%%%%%%%%%%
cHSV=cat(3,hol,sat,val_new);
nHSV=im2double(cHSV);
RGB=hsv2rgb(nHSV);
nRGB=im2uint8(RGB);
imwrite(nRGB, file_output);


