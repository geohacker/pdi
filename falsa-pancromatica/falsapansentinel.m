clc;
clear;

%Leer Imagen
filename = 'subset-rgb432-boa';
[sentinel,R] = geotiffread(filename);
info = geotiffinfo(filename);
%Desacoplar Bandas
r = double(sentinel(:,:,1));
g = double(sentinel(:,:,2));
b= double(sentinel(:,:,3));
%Normalizacion - Falsa Pancromatica
f_pan = fix(r+g+b*(1/3)); 
f_pan8bits  = uint16(f_pan)
%Generar Nueva Imagen
filename_dest = 'sentinel-fpan-boa.tif'
geotiffwrite(filename_dest, f_pan8bits, R, ...
    'GeoKeyDirectoryTag' ,  info.GeoTIFFTags.GeoKeyDirectoryTag  );
%Visualizar  Nueva Imagen
figure, imshow(filename_dest);
